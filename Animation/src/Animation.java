import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Animation extends JPanel implements ActionListener {

    Timer tm = new Timer(5, this);
    int y = 0, velY = 5;

    //nakljucno izberemo tip kupona (0,1,2,3,4)
    Random random = new Random();
    int couponType = random.nextInt(5);

    //definiramo barve
    Color purple = new Color(153, 0, 76);
    Color lightGreen = new Color(153, 250, 0);
    Color turkis = new Color(40, 183, 141);
    Color yellow = new Color(255, 254, 89);
    Color red = new Color(205, 92, 92);
    Color beige = new Color(245, 213, 188);
    Color ozadje = new Color(204, 255, 204);
    Color text = new Color(102, 0, 51);

    Color[] ColorArray = {purple, lightGreen, turkis, yellow, red}; //barve kuponov

    int[] counters = {0, 0, 0, 0, 0}; //stevci kuponov

    boolean end = false;
    boolean first = false;
    
    //dobimo dimenzije zaslona
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final int screenHeight = screenSize.height;
    final int screenWidth = screenSize.width;

    //spremenljivke visin in sirin
    int a = screenWidth / 9;
    int b = screenWidth - screenWidth / 9;
    int dimSkatle = (b - a) / 7;

    int x_rect2 = a + 3 * (b - a) / 14;
    int x_rect3 = a + 6 * (b - a) / 14;
    int x_rect4 = a + 9 * (b - a) / 14;
    int x_rect5 = a + 12 * (b - a) / 14;

    int visinaPodlage = screenHeight / 13;
    int y_rect = screenHeight - dimSkatle;

    int y_text = screenHeight - visinaPodlage - 10;
    int dimKupona = 50;

    //pozicije kuponov
    int centerKupona = (dimSkatle - dimKupona) / 2;
    int[] positions = {a + centerKupona, x_rect2 + centerKupona, x_rect3 + centerKupona, x_rect4 + centerKupona, x_rect5 + centerKupona};

    public void paintComponent(Graphics g) {//metoda za risanje
        super.paintComponent(g);
        this.setBackground(ozadje);

        //KUPON
        if(first){ //kupon se narise s klikom
        	g.setColor(ColorArray[couponType]);
            g.fillRect(positions[couponType], y, dimKupona, dimKupona);
        }
        
        //SKATLE
        g.setFont(new Font("Lucida Sans", Font.BOLD, 18)); //pisavo je potrebno spremeniti glede na velikost zaslona

        g.setColor(purple);
        g.fillRect(a, y_rect, dimSkatle, dimSkatle);
        g.setColor(Color.BLACK);
        g.drawString("Število kuponov: " + Integer.toString(counters[0]), a + dimSkatle / 20, y_text);

        g.setColor(lightGreen);
        g.fillRect(x_rect2, y_rect, dimSkatle, dimSkatle);
        g.setColor(Color.BLACK);
        g.drawString("Število kuponov: " + Integer.toString(counters[1]), x_rect2 + dimSkatle / 20, y_text);

        g.setColor(turkis);
        g.fillRect(x_rect3, y_rect, dimSkatle, dimSkatle);
        g.setColor(Color.BLACK);
        g.drawString("Število kuponov: " + Integer.toString(counters[2]), x_rect3 + dimSkatle / 20, y_text);

        g.setColor(yellow);
        g.fillRect(x_rect4, y_rect, dimSkatle, dimSkatle);
        g.setColor(Color.BLACK);
        g.drawString("Število kuponov: " + Integer.toString(counters[3]), x_rect4 + dimSkatle / 20, y_text);

        g.setColor(red);
        g.fillRect(x_rect5, y_rect, dimSkatle, dimSkatle);
        g.setColor(Color.BLACK);
        g.drawString("Število kuponov: " + Integer.toString(counters[4]), x_rect5 + dimSkatle / 20, y_text);

        //PODLAGA 
        g.setColor(beige);
        g.fillRect(a / 2, screenHeight - visinaPodlage, screenWidth - a, visinaPodlage);

        //izpisemo tekst, ko je konec
        if (end) {
            int sum = 0;
            for (int i = 0; i < counters.length; i++) {
                sum += counters[i]; //izracunamo stevilo vseh prejetih kuponov
            }
            g.setColor(text);
            g.setFont(new Font("Lucida Sans", Font.PLAIN, 40));
            g.drawString("Število vseh prejetih kuponov je " + sum + ".", 200, 200);
            tm.stop(); //ustavimo timer
        }

    }

    public void actionPerformed(ActionEvent e) {
        //ko kupon "pade v skatlo"
        if (y > y_rect) {
            counters[couponType]++; //povecamo stevec
            //pogledamo, ce je katera skatla se prazna - ce ni, je konec
            if (counters[0] > 0 && counters[1] > 0 && counters[2] > 0 && counters[3] > 0 && counters[4] > 0) {
                end = true;
                System.out.println("konec");
            } else {//ce se ni konec, se y koordinata kupona spremeni v 0 - nov kupon se pojavi na vrhu zaslona
                y = 0;
                couponType = random.nextInt(5); //ponovno nakljucno izberemo tip novega kupona
            }
        }

        y = y + velY; //padanje kupona; velY je hitrost padanja

        repaint();

    }

    public static void main(String[] args) {

        final Animation coupons = new Animation();
        JFrame jf = new JFrame();
        jf.setTitle("Coupon Collector");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        jf.setSize(screenSize.width, screenSize.height);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.add(coupons);
        coupons.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {//klik na misko sprozi premikanje kupona
                coupons.tm.start();
                coupons.first = true;
                coupons.end = false;
                for (int i = 0; i < coupons.counters.length; i++) { //ponastavimo stevce
                	coupons.counters[i] = 0;
                }
                coupons.y = 0;        
            }
        });
    }

}