# COUPON COLLECTOR #

* This is a simple (Java) animation for Coupon Collector's problem, where coupons come randomly.

* See Animation.java for code or Animation.jar for view app only.

![Screen Shot 2015-09-21 at 14.51.17.png](https://bitbucket.org/repo/jjEndg/images/3988076959-Screen%20Shot%202015-09-21%20at%2014.51.17.png)

![Screen Shot 2015-09-21 at 14.51.05.png](https://bitbucket.org/repo/jjEndg/images/2931060282-Screen%20Shot%202015-09-21%20at%2014.51.05.png)

* For more information about the problem see [https://en.wikipedia.org/wiki/Coupon_collector%27s_problem](https://en.wikipedia.org/wiki/Coupon_collector%27s_problem).

* (I'm new to java so any suggestions about this code are very welcome! :) )